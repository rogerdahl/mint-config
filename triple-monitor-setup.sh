#!/bin/sh

# Configure desktop display over multiple monitors

# - Note! XRandR and NVIDIA drivers don't play well together.
# - Seem to need to specify either --panning or --fbmm for things to work
# - Could not get plain --pos to work
# - Could not get relative positioning (--left-of, --right-of) to work
# - The shell script created by arandr (.screenlayout.sh) does not configure the monitors correctly
#
# Unedited output from arandr:
#
# - Does not work. Causes lots of overlapping areas between monitors
#xrandr --output DP-0 --off --output DVI-D-0 --mode 1920x1080 --pos 0x248 --rotate left --output DVI-D-1 --mode 1920x1080 --pos 4920x240 --rotate left --output DP-1 --off --output HDMI-0 --primary --mode 3840x2160 --pos 1080x0 --rotate normal
#
# NVIDIA
#
# These values show in the NVIDIA X Server Settings app when the monitors are
# configured the way I want them.
#
#Left:
# Rotate: Left
# +0+0
# ViewPortIn: 1620x2880
# ViewPortOut:1920x1080+0+0
# Panning:1620x2880
#
#Center:
# Rotate: No
# +1620+728
# ViewPortIn: 3840x2160
# ViewPortOut:3840x2160+0+0
# Panning:3840x2160
#
#Right:
# Rotate: Left
# +5460+0
# ViewPortIn: 1620x2880
# ViewPortOut:1920x1080+0+0
# Panning:1620x2880
#
# When defining scaling as pixel resolutions:
# 1920x1080 * 1.5 (scale) = 2880x1620 (value that appears below)
#
# Some kind of reset that makes things work?
# https://askubuntu.com/questions/518757/xrandr-not-working-with-nvidia
#xrandr -s 0

# Set up monitors using absolute positions and pixel defined scaling, 4K monitor connected by HDMI
#xrandr \
#--dpi 180 \
#--output HDMI-0  --mode 3840x2160 --panning 3840x2160+1620+728                        --rate 30 --rotate normal \
#--output DVI-D-1 --mode 1920x1080 --panning 1620x2880+5460+0   --scale-from 2880x1620 --rate 60 --rotate left \
#--output DVI-D-0 --mode 1920x1080 --panning 1620x2880+0+0      --scale-from 2880x1620 --rate 60 --rotate left

# Set up monitors using absolute positions and pixel defined scaling, 4K monitor connected by DisplayPort
xrandr \
--dpi 180 \
--output DP-0    --mode 3840x2160 --panning 3840x2160+1620+728                        --rate 60 --rotate normal \
--output DVI-D-1 --mode 1920x1080 --panning 1620x2880+5460+0   --scale-from 2880x1620 --rate 60 --rotate left \
--output DVI-D-0 --mode 1920x1080 --panning 1620x2880+0+0      --scale-from 2880x1620 --rate 60 --rotate left



# Set up monitors using absolute positions and pixel defined scaling
#xrandr \
#--output HDMI-0  --mode 3840x2160 --panning 3840x2160+1620+728                        --rate 30 #--rotate normal \
#--output DVI-D-1 --mode 1920x1080 --panning 1620x2880+5460+0   --scale-from 2880x1620 --rate 60 #--rotate left \
#--output DVI-D-0 --mode 1920x1080 --panning 1620x2880+0+0      --scale-from 2880x1620 --rate 60 #--rotate left

#xrandr \
#--output HDMI-0  --mode 3840x2160 --panning 3840x2160+1620+728 --scale 2x2            --rate 30 --rotate normal \
#--output DVI-D-1 --mode 1920x1080 --panning 1620x2880+5460+0                          --rate 60 --rotate left \
#--output DVI-D-0 --mode 1920x1080 --panning 1620x2880+0+0                             --rate 60 --rotate left

# Some stuff that didn't work, for reference
#
# This didn't work. --pos had to go as --panning values
#xrandr \
#--output HDMI-0   --mode 3840x2160 --pos 1620x728  --panning 3840x2160 --rate 30 --rotate normal --primary \
#--output DVI-D-0  --mode 1920x1080 --pos 0x0       --panning 2880x1620 --rate 60 --rotate left \
#--output DVI-D-1  --mode 1920x1080 --pos 5460x0    --panning 2880x1620 --rate 60 --rotate left
#
# --scale-from 1920x1080
# --scale-from 1920x1080 --right-of HDMI-0
#
#xrandr \
#--output HDMI-0   --mode 3840x2160 --pos 0x0                         --rate 30 --rotate normal --primary --crtc 0 \
#--output DVI-D-0  --mode 1920x1080 --left-of HDMI-0  --scale 1.5x1.5 --rate 60 --rotate left --crtc 0 \
#--output DVI-D-1  --mode 1920x1080 --right-of HDMI-0 --scale 1.5x1.5 --rate 60 --rotate left --crtc 0

#xrandr \
#--output DVI-D-0 --scale 1.5x1.5 --pos 0x0      --fbmm 1920x1080 --rate 60 --rotate left \
#--output HDMI-0                  --pos 1620x720 --fbmm 3840x2160 --rate 30 --rotate normal --primary \
#--output DVI-D-1 --scale 1.5x1.5 --pos 5460x0   --fbmm 1920x1080 --rate 60 --rotate left
