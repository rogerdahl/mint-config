## Linux Mint Setup Notes

Notes and scripts for setting up Linux Mint.

### After install:

/etc/sudoers

	Defaults always_set_home

Remove anoying alt-tab preview (window border, dimming, flickering):

    $ gsettings set org.mate.Marco.general show-tab-border false

# Triple monitor setup

- Mint Cinnamon 19.3 with NVIDIA GTX 1060 and manually compiled and installed drivers
- This setup enables combining monitors with different resolutions while text and graphics is displayed at the same physical size on all monitors
- Text and graphics is sharper on the higher resolution monitor
- I no longer need to run any xrandr commands!
- The Using X and the NVIDIA driver directly to set up the monitors
- Cinnamon doesn't understand that screens on the same desktop can have different resolutions, so we're settings things up directly in X and the NVIDIA driver.
- Cinnamon just sees a single screen with a single DPI

- If QT programs ignore all `QT_*` environment variables and scaling settings:
    - `gnome-settings-daemon` may be setting an invalid value in `Xft.dpi`. Check with `xrdb -query`. Only integer values are allowed. If it's a float, it's invalid, and must be updated.
    - Mint Cinnamon is apparently not very good at following standard practices for sourcing files like `~/.xinitrc` and `~/.xsession`
    - So, to update `Xft.dpi` after it's set by `gnome-settings-daemon`, create a link in `~/.config/autostart` that points to `load-xresources.desktop`.
    - Background on sourced files: https://unix.stackexchange.com/a/281923/21709 (and other answers in that Q)

- NVIDIA settings:

    - All monitors:
        - Force Composition Pipeline & Force Full Composition Pipeline: OFF

    - Side monitors:
        - ViewPortIn & Panning: 1620x2880

# Keyboard

## Cinnamon

$ gsettings set org.cinnamon.settings-daemon.peripherals.keyboard delay 250
$ gsettings set org.cinnamon.settings-daemon.peripherals.keyboard repeat-interval 15

## MATE

$ gsettings set org.mate.peripherals-keyboard delay 250
$ gsettings set org.mate.peripherals-keyboard rate 75

# vim

$ mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
$ cd ~/.vim/bundle && git clone https://github.com/tpope/vim-sensible.git
$ git clone git@github.com:dracula/vim.git bundle/dracula

    execute pathogen#infect()

    syntax on
    filetype plugin indent on

    if (has("termguicolors"))
      set termguicolors
    endif

    colorscheme dracula

    # ctermbg uses the palette on 256-color terminals. guibg is truecolor and used when vim is drawing to a local GUI
    hi Normal ctermbg=234 guibg=#191A21

    set cursorline 
    hi CursorLine ctermbg=234 guibg=#191A21

    autocmd InsertEnter * hi CursorLine ctermbg=236 guibg=#292a31
    autocmd InsertLeave * hi CursorLine ctermbg=234 guibg=#191a21

    set tabstop=4
    set shiftwidth=4
    set expandtab


### Keyboard shortcuts

- By default, the OS captures a lot of shortcuts that I want to go to the IDE.

- Unicode Compose
	- Ctrl+Shift+u is the shortcut for Unicode Compose. Following it with a four digit number will cause that Unicode Code Point to be inserted.
        - To disable: System Settings > Input Methods > Input Method Framework (in the title bar, sigh) > Set to XIM

- Free alt + left mouse click for use to create multiple carrets in PyCharm
- Keywords: alt drag window

  MATE: dconf-editor > org > mate > marco > mouse-button-modifier: <Super>
  Cinnamon: System Settings > Windows > Behavior > Special Key to move and resize windows: Super

# Version:

$ cat /etc/linuxmint/info

# Virtual Terminals

Enable switching VTs only with SysRq magic kernel escape sequences, freeing the Ctrl+Alt+F1 - F6 shortcuts up for use by programs.

With these changes, First do Alt+SysRq+r (Only Alt has to be pressed for the full sequence), then the regular Ctrl+Alt+Fx sequence will work for swithing to a VT. I don't know if the Ctrl+Alt+Fx can be disabled again without rebooting. It may cause other changes in how the keyboard works as well.

/etc/X11/xorg.conf

    Section "ServerFlags"
        Option         "DontVTSwitch" "on"
    EndSection

    Section "InputClass"
        Identifier "keyboard defaults"
        MatchIsKeyboard "on"
        Option "XKbOptions" "srvrkeys:none"
    EndSection

/etc/sysctl.d/10-magic-sysrq.conf

    kernel.sysrq = 1
    
Reboot

## Background info

https://en.m.wikipedia.org/wiki/Magic_SysRq_key
https://wiki.debian.org/Keyboard
https://unix.stackexchange.com/a/326804/21709

Re. the xorg.conf changes:

The srvrkeys:none option points to a rule +srvr_ctrl(no_srvr_keys) in /usr/share/X11/xkb/rules/evdev which points to a rule in /usr/share/X11/xkb/symbols/srvr_ctrl which suppresses the standard interpretation of the Ctrl-Alt-Fn key strokes.

It's possible to reduce the number of VTs that can be opened, but modern Linux, VTs are only created when opened, so there's probably no point. For more info, search online for:

/etc/console-setup/remap.inc
sudo dpkg-reconfigure console-setup -phigh
/etc/systemd/logind.conf

### Resources

https://wiki.archlinux.org/index.php/Multihead

https://unix.stackexchange.com/questions/137844/per-screen-font-config

https://winaero.com/blog/find-change-screen-dpi-linux/
https://www.mythtv.org/wiki/Specifying_DPI_for_NVIDIA_Cards
https://forums.linuxmint.com/viewtopic.php?t=248113

https://awesomewm.org/

https://unix.stackexchange.com/questions/80774/separate-workspaces-on-each-monitor
https://superuser.com/questions/637382/using-keyboard-shorcuts-with-the-windows-key-in-linux-mint-mate/889528#889528
https://github.com/linuxmint/mintmenu/issues/98
https://ubuntuforums.org/showthread.php?t=2316105
https://wiki.ubuntuusers.de/SDDM/

https://askubuntu.com/questions/743473/how-to-disable-monitor-auto-configuration
https://www.reddit.com/r/linuxmint/comments/3ce881/cinnamon_resets_nvidia_x_server_settings/
https://forums.linuxmint.com/viewtopic.php?t=244489

https://forums.linuxmint.com/viewtopic.php?t=228159
