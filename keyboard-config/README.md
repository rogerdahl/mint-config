# Configure keyboard shortcuts in Mint Cinnamon and MATE


## Python

```shell script
pyenv virtualenv 3.8.2 keyboard-config
pyenv local keyboard-config 
sudo apt install libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev gir1.2-gtk-3.0
pip install wheel pycairo PyGObject
```

## Shell

```shell script
sudo apt install dconf-editor
dconf-editor
```
