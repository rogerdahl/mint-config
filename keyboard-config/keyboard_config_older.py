#!/usr/bin/env python

# https://developer.gnome.org/gio//2.50/GSettings.html
# https://pygobject.readthedocs.io/en/latest/getting_started.html
# https://lazka.github.io/pgi-docs/Gio-2.0/index.html#
# sudo apt install libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev gir1.2-gtk-3.0
# pip install pycairo PyGObject


# GLib is the low-level core library that forms the basis for projects such as GTK+ and GNOME. It provides data
# structure handling for C, portability wrappers, and interfaces for such runtime functionality as an event loop,
# threads, dynamic loading, and an object system.

# GConf:
# - GNOME 2.x configuration system
#
# DConf:
# - Replacement for GConf
# - GMONE 3.x ?
# - Gsettings is a development library used to read and write to a configuration store backend. On Linux, it uses Dconf,
# but on Windows, it uses the registry, and on OS X, it uses a native data store.
# - The gsettings command line tool is simply a tool to access or modify settings via the GSettings API Application
# developers and end-users are recommended to use Gsettings, not Dconf directly.


import gi.repository.Gio
import gi.repository.GLib


Super, Shift, Ctrl, Alt = range(4)

WINDOW_KEYBINDINGS_SCHEMA_LIST = [
    # "org.mate.Marco.global-keybindings"
    # "org.mate.Marco.window-keybindings"
    # "org.cinnamon.desktop.keybindings"
    # "org.cinnamon.desktop.keybindings.custom-keybinding",
    # "org.cinnamon.keybindings.custom-keybinding",
    # "org.cinnamon.desktop.keybindings.media-keys",
    "org.cinnamon.desktop.keybindings.wm",
    # "org.cinnamon.keybindings",
    # "org.cinnamon.muffin.keybindings",
    # "org.gnome.desktop.wm.keybindings",
    # "org.gnome.metacity.keybindings",
]


SHORTCUT_DICT = {
    # Switch
    "switch-windows": (Alt, "Tab"),
    "switch-windows-backward": (Alt, Shift, "Tab"),
#    "switch-panels": (Ctrl, "Tab"),
#    "switch-panels-backward": (Ctrl, Shift, "Tab"),
    "switch-group": (Alt, Ctrl, "Above_Tab"),
    "switch-group-backward": (Alt, Ctrl, Shift, "Above_Tab"),
    # Other Alt
    "activate-window-menu": (Alt, "space"), # (Super, 'space') does not work
    "close": (Alt, "F4"),
    # Misc
    "begin-move": (Super, "F1"),
    "begin-resize": (Super, "F2"),
    "panel-run-dialog": (Super, "F3"),
    "show-desktop": (Super, "d"),
    # Maximize
    "toggle-maximized": (Super, "F10"),
    "maximize": (Super, Shift, "F9"),
    "maximize-horizontally": (Super, "F9"),
    "maximize-vertically": (Super, "F8"),
    # Move to monitor
    "move-to-monitor-left": (Super, Shift, "Left"),
    "move-to-monitor-right": (Super, Shift, "Right"),
    "move-to-monitor-up": (Super, Shift, "Up"),
    "move-to-monitor-down": (Super, Shift, "Down"),
    # Move to side within monitor without resize
    "move-to-side-w": (Super, Alt, "Left"),
    "move-to-side-e": (Super, Alt, "Right"),
    "move-to-side-n": (Super, Alt, "Up"),
    "move-to-side-s": (Super, Alt, "Down"),
    # Move to side within monitor with tiling and resize
    "push-snap-left": (Super, Ctrl, "Left"),
    "push-snap-right": (Super, Ctrl, "Right"),
    "push-snap-up": (Super, Ctrl, "Up"),
    "push-snap-down": (Super, Ctrl, "Down"),
    # "push-tile-left": (Super, Shift, Ctrl, "Left"),
    # "push-tile-right": (Super, Shift, Ctrl, "Right"),
    # "push-tile-up": (Super, Shift, Ctrl, "Up"),
    # "push-tile-down": (Super, Shift, Ctrl, "Down"),
    # Move window to monitor
    "move-to-corner-nw": (Super, Alt, Ctrl, "Left"),
    "move-to-corner-ne": (Super, Alt, Ctrl, "Right"),
    "move-to-corner-sw": (Super, Alt, Ctrl, "Up"),
    "move-to-corner-se": (Super, Alt, Ctrl, "Down"),
    # "move-to-center": (Super, Alt, Ctrl, "Up"),
    # 'move-to-corner-se', (''),
    # 'move-to-corner-sw', (''),
    # 'move-to-monitor-e', (''),
    # 'move-to-monitor-n', (''),
    # 'move-to-monitor-s', (''),
    # 'move-to-monitor-w', (''),
}


def main():
    # print('~' * 100)
    # print('BEFORE')
    # dump_shortcuts_for_all_schemas()
    # disable_shortcuts_for_all_schemas()
    # set_my_shortcuts_for_all_schemas()
    # print('~' * 100)
    # print('AFTER')
    configure_shortcuts()
    dump_shortcuts_for_all_schemas()


def dump_shortcuts_for_all_schemas(only_modified=False, ignore_disabled=False):
    print("#" * 100)
    for schema_id in WINDOW_KEYBINDINGS_SCHEMA_LIST:
        dump_shortcuts_for_schema(schema_id, only_modified, ignore_disabled)
    print("#" * 100)


def dump_shortcuts_for_schema(schema_id, only_modified=False, ignore_disabled=False):
    print("-" * 100)
    print("Shortcuts")
    print("only_modified: {}".format(only_modified))
    print("ignore_disabled: {}".format(ignore_disabled))
    print("schema: {}".format(schema_id))
    print()

    gnome_shortcut_config = GnomeShortcutConfig(schema_id)
    shortcut_dict = gnome_shortcut_config.get_shortcuts_dict(
        only_modified, ignore_disabled
    )
    for k in sorted(shortcut_dict.keys()):
        print(
            "{:<30} default: {:<30} current: {:<30}".format(
                k, shortcut_dict[k]["default_keys"], shortcut_dict[k]["current_keys"]
            )
        )
    print()


def configure_shortcuts():
    print("#" * 100)
    for schema_id in WINDOW_KEYBINDINGS_SCHEMA_LIST:
        configure_shortcuts_for_schema(schema_id)
    print("#" * 100)


def configure_shortcuts_for_schema(schema_id):
    """Set keyboard shortcuts defined in SHORTCUT_DICT and disable all other shortcuts.
    Note: I had weird issues where updates I made to the shortcuts would show up in
    dconf-editor but would not activate even after a reboot. What seems to work is to
    set and clear shortcuts in a single pass, instead of in separate passes. That I'm
    using multiple config classes might help as well.
    """
    print("-" * 100)
    print("Setting shortcuts in schema: {}".format(schema_id))
    gnome_shortcut_config = GnomeShortcutConfig(schema_id)
    available_shortcut_dict = gnome_shortcut_config.get_shortcuts_dict()
    for avail_shortcut_name in available_shortcut_dict.keys():
        if avail_shortcut_name in SHORTCUT_DICT.keys():
            gnome_shortcut_config2 = GnomeShortcutConfig(schema_id)
            gnome_shortcut_config2.set_shortcut_keys(
                avail_shortcut_name,
                gnome_shortcut_config.format_shortcut_keys(
                    SHORTCUT_DICT[avail_shortcut_name][-1],
                    *SHORTCUT_DICT[avail_shortcut_name][:-1],
                ),
            )
        else:
            gnome_shortcut_config3 = GnomeShortcutConfig(schema_id)
            gnome_shortcut_config3.disable_shortcut(avail_shortcut_name)


def set_my_shortcuts_for_all_schemas():
    print("#" * 100)
    print("Setting my shortcuts")
    for schema_id in WINDOW_KEYBINDINGS_SCHEMA_LIST:
        set_my_shortcuts(schema_id)
    print("#" * 100)


def set_my_shortcuts(schema_id):
    print("-" * 100)
    print("Setting my shortcuts in schema: {}".format(schema_id))
    gnome_shortcut_config = GnomeShortcutConfig(schema_id)
    available_shortcut_dict = gnome_shortcut_config.get_shortcuts_dict()
    for shortcut_name, new_keys in MY_SHORTCUT_LIST:
        if shortcut_name in available_shortcut_dict.keys():
            gnome_shortcut_config.set_shortcut_keys(
                shortcut_name,
                gnome_shortcut_config.format_shortcut_keys(
                    new_keys[-1], *new_keys[:-1]
                ),
            )
        else:
            print(
                "{}: Shortcut not available in schema: {}".format(
                    shortcut_name, schema_id
                )
            )


# noinspection PyUnresolvedReferences
class GnomeShortcutConfig:
    def __init__(self, schema_id=None, schema_dir=None):
        self.schema_id = schema_id
        self.schema_dir = schema_dir
        self.schema_source = self.get_schema_source()
        # print(self.schema_source)
        self.gsettings_schema = self.get_gsettings_schema()
        # print(self.gsettings_schema)
        self.gsettings = self.get_gsettings()
        # print(dir(self.gsettings))
        # self.get_schema_list()

    def get_schema_source(self):
        if self.schema_dir is None:
            schema_source = gi.repository.Gio.SettingsSchemaSource.get_default()
        else:
            schema_source = gi.repository.Gio.SettingsSchemaSource.new_from_directory(
                self.schema_dir,
                gi.repository.Gio.SettingsSchemaSource.get_default(),
                False,
            )
        return schema_source

    def get_gsettings_schema(self):
        gsettings_schema = self.schema_source.lookup(self.schema_id, True)
        if not gsettings_schema:
            raise ShortcutError(
                'Schema lookup failed. schema_id="{}"'.format(self.schema_id)
            )
        return gsettings_schema

    def get_gsettings(self):
        return gi.repository.Gio.Settings(settings_schema=self.gsettings_schema)

    def get_schema_list(self):
        for schema_list in self.schema_source.list_schemas(True):
            return schema_list

    def get_shortcuts_dict(self, only_modified=False, ignore_disabled=False):
        """Return the shortcut name (schema key), current and default keys
        for each shortcut in the schema, optionally filtered on modified and/or
        disabled status
        """
        d = {}
        for k in self.gsettings.list_keys():
            default_keys, current_keys = self.get_default_and_current_keys(k)
            if not only_modified or (only_modified and default_keys != current_keys):
                if not ignore_disabled or (
                    ignore_disabled and not self.is_disabled_keys(current_keys)
                ):
                    d[k] = {"default_keys": default_keys, "current_keys": current_keys}
        return d

    def get_default_and_current_keys(self, shortcut_name):
        """Return the current and default keys for a single shortcut name"""
        return (
            self.get_str(self.gsettings.get_default_value(shortcut_name)),
            self.get_str(self.gsettings.get_value(shortcut_name)),
        )

    def set_shortcut_keys(self, shortcut_name, new_keys):
        """Set new keys for a shortcut"""
        default_keys, current_keys = self.get_default_and_current_keys(shortcut_name)
        if current_keys == new_keys:
            print("{}: Already set to value: {}".format(shortcut_name, current_keys))
            return
        self.gsettings.set_value(
            shortcut_name, gi.repository.GLib.Variant.new_strv([new_keys])
        )
        print(
            "{}: Updated (default: {}) {} -> {}".format(
                shortcut_name, default_keys, current_keys, new_keys
            )
        )

    def disable_shortcut(self, shortcut_name):
        default_keys, current_keys = self.get_default_and_current_keys(shortcut_name)
        if current_keys == "disabled":
            print("{}: Already disabled".format(shortcut_name))
            return
        self.set_shortcut_keys(shortcut_name, "disabled")

    def get_str(self, gsettings_value):
        if len(gsettings_value):
            return gsettings_value[0]
        return ""

    def is_disabled_shortcut(self, shortcut_name):
        return self.is_disabled_keys(
            self.get_default_and_current_keys(shortcut_name)[1]
        )

    def is_disabled_keys(self, keys):
        return keys == "disabled"

    def format_shortcut_keys(self, char_key, *mod_keys):
        """Return a string that specifies the keys for the shortcut
        - The shortcut key string parser in Gnome handles various formats, so
        there are many other valid representations of a given key combination.
        """
        return "".join(
            [
                "<Super>" if Super in mod_keys else "",
                "<Shift>" if Shift in mod_keys else "",
                "<Control>" if Ctrl in mod_keys else "",
                "<Alt>" if Alt in mod_keys else "",
                char_key,
            ]
        )


class ShortcutError(Exception):
    pass


if __name__ == "__main__":
    main()
